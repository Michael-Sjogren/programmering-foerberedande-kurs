using System;
using System.IO;
using System.Collections.Generic;

namespace programmering_foerberedande_kurs
{
    class Program
    {
        static void Main(string[] args)
        {
            new Program().RunProgram();
        }
        void RunProgram()
        {
            String input;
            Console.WriteLine("Welcome to the menu, press 'h' for a list of commands.");
            while( (input = Console.ReadLine().ToLower().Trim()) != "quit")
            {
                Console.Clear();
                Console.WriteLine("Welcome to the menu, press 'h' for a list of commands.");
                switch(input)
                {
                    case "":
                        break;
                    case "1":
                        Console.Clear();
                        Console.WriteLine("Hello World");
                        break;
                    case "2":
                        PrintInputtedNameAndAge();
                        break;
                    case "3":
                        ToggleConsoleColor();
                        break;
                    case "4":
                        PrintCurrentDateToConsole();
                        break;
                    case "5":
                        PrintGreaterInputtedValue();
                        break;
                    case "6":
                        GuessRandomNumber();
                        break;
                    case "7":
                        WriteToFile();
                        break;
                    case "8":
                        ReadFromFile();
                        break;
                    case "9":
                        SqrtAndPOW();
                        break;
                    case "10":
                        PrintMultiplicationTable();
                        break;
                    case "11":
                        SortRandomNumArrayAcending();
                        break;
                    case "12":
                        Palindrome();
                        break;
                    case "13":
                        AllNumbersBetweenRange();
                        break;
                    case "14":
                        CommaSeparatedNumbers();
                        break;
                    case "15":
                        AdditionCommaSeparatedNumbers();
                        break;
                    case "16":
                        CreateTwoCharacthers();
                        break;
                    case "h":
                        Console.WriteLine("Type 'quit' to quit. type a number from 1 to 16 to run the tasks.");
                        break;
                    default:
                        Console.WriteLine("Unrecognized input command. type in 'h' for help.");
                        break;
                }
            }
            Console.Clear();
            Console.WriteLine("Shutting down program...");
        }

        // task 2
        void PrintInputtedNameAndAge()
        {
            Console.Clear();
            Console.WriteLine("Task 2");
            Console.Write("Your first name: ");
            String firstName = Console.ReadLine().Trim();
            Console.WriteLine();
            Console.Write("Your last name: ");
            String lastName = Console.ReadLine().Trim();
            Console.WriteLine();
            Console.Write("Your age: ");
            try {
                int age = int.Parse(Console.ReadLine());
                Console.WriteLine("Result: " + firstName  + " "+ lastName + ", " + age);
            }catch(FormatException)
            {
                Console.WriteLine("Invalid age, try again. returning to menu.");
            }

            
        }
        // task 3
        void ToggleConsoleColor()
        {
            Console.Clear();
            Console.WriteLine("Task 3");
            if (Console.ForegroundColor == ConsoleColor.DarkGreen)
            {
                Console.ResetColor();
                Console.WriteLine("Toggling console color back to default");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Toggling console color to green");
            }
        }
        // task 4
        void PrintCurrentDateToConsole()
        {
            Console.Clear();
            Console.WriteLine("Task 4");
            Console.WriteLine("Current date: " + DateTime.Today.ToShortDateString() );
        }

        // task 5

        void PrintGreaterInputtedValue()
        {
            Console.Clear();
            Console.WriteLine("Task 5 , input two numbers and see what number is greatest");
            try {
                Console.Write("Number 1: ");
                double num1 = double.Parse(Console.ReadLine());
                Console.WriteLine();
                Console.Write("Number 2: ");
                double num2 = double.Parse(Console.ReadLine());

                Console.WriteLine( "The greater number is: " + ( (num1 > num2) ? num1 : num2 ) );
            }catch(FormatException)
            {
                Console.WriteLine("Not a number, try again. returning to menu.");
            }
        }

        // task 6
        void GuessRandomNumber()
        {
            int guesses = 0;
            int randomNum = new Random().Next(1,100);
            Console.Clear();
            Console.WriteLine("Task 6 , guess a whole number between 1 - 100");
            String input;
            while( (input = Console.ReadLine()) != randomNum.ToString() )
            {
                try 
                {
                    int guess = int.Parse(input);
                    guesses += 1;
                    Console.WriteLine( (guess > randomNum) ? "Lower" : "Higher" );
                }
                catch(FormatException)
                {
                    Console.WriteLine("Invalid number format, enter only integers between 1 - 100.");
                }
            }
            Console.WriteLine("You guessed correctly!" + "\nIt only took you " + guesses + " try(s).");
        }

        // task 7
        void WriteToFile()
        {
            Console.Clear();
            Console.WriteLine("Task 7 , enter some text that will be written to a file.");
            String text = Console.ReadLine();
            File.WriteAllText("file.txt",text);
        }

        // task 8
        void ReadFromFile()
        {
            Console.Clear();
            Console.WriteLine("Task 8 , Read the contents of a file.");
            if(!File.Exists("file.txt"))
            {
                Console.WriteLine("file does not exist, create a file by doing task 7 first.");
                return;
            }
            foreach (var line in File.ReadAllLines("file.txt"))
            {
                Console.WriteLine(line);
            }
        }

        // task 9
        void SqrtAndPOW()
        {
            Console.Clear();
            Console.WriteLine("Enter a decimal number");
            String input = Console.ReadLine();
            double num  = 0.0;
            try {
                num = double.Parse(input);
            }
            catch(FormatException)
            {
                Console.WriteLine("Not a number");
                return;
            }
            Console.WriteLine( Math.Pow(Math.Pow(Math.Sqrt(num) , 2),10) );
        }

        // task 10
        void PrintMultiplicationTable()
        {
            Console.Clear();
            Console.WriteLine("Task 10, The Multiplication table.");
            for(int col = 1; col <= 10; col++)
            {
                for(int row = 1; row <= 10; row++)
                {
                    Console.Write( ""+(row*col) +"\t" );
                }
                Console.WriteLine("");
                Console.WriteLine("");
            }
        
        }
        // task 11
        void SortRandomNumArrayAcending()
        {
            Console.Clear();
            Console.WriteLine("Task 11 , Sort random list");
            const int maxSize = 10;
            List<int> randomNums = new List<int>(maxSize);
            // create array of random nums
            Console.WriteLine("Random numbers list:");
            for(int i = 0; i < maxSize; i++)
            {
                randomNums.Add(new Random().Next(-1000 , 1000));
                Console.Write(randomNums[i] + " ");
            }
            Console.WriteLine("\nSorted list:");

            var newArray = new List<int>(randomNums);
            newArray.Sort();

            foreach(int num in newArray){
                Console.Write(num + " ");
            }
        }
        // task 12
        void Palindrome()
        {
            Console.Clear();
            Console.WriteLine("Task 12 , enter a word and the console will print true if it is a palindrome and false if it is not.");
            Console.Write("Input: ");
            String input = Console.ReadLine().ToLower().Trim();
            if (input == "")
            {
                Console.WriteLine("False");
                return;
            }
            var chars = input.ToCharArray();
            Array.Reverse(chars);
            String reversed = new String(chars);
            Console.WriteLine( reversed.Equals(input) ? "True" : "False" );
        }

        // task 13

        void AllNumbersBetweenRange()
        {
            Console.Clear();
            Console.WriteLine("Task 13 , display the numbers between two inputted values");
            try
            {
                int num1 = int.Parse(Console.ReadLine());
                int num2 = int.Parse(Console.ReadLine());

                int max = Math.Max(num1 , num2);
                int min = Math.Min(num1, num2);
                Console.WriteLine("Printing the numbers between " + min + " and " + max);
                int j = 0;
                for (int i = min +1; i < max; i++ , j++)
                {
                    if (j % 10 == 0)
                    {
                        Console.WriteLine();
                    }
                    Console.Write(i + " ");
                }
            }
            catch(FormatException)
            {
                Console.WriteLine("Not a number. Returning to menu.");
            }
        }
        // task 14
        void CommaSeparatedNumbers()
        {
            Console.Clear();
            Console.WriteLine("Task 14 ");
            Console.WriteLine("Enter a list of numbers with a comma as a seperator.");
            Console.WriteLine("Example: 1,2,3,4,5,6,7,8,9,10");
            String input = Console.ReadLine();
            if (input == null) {
                Console.Write("Nothing inputted , returning to menu.");
                return;
            }

            String[] strings = input.Trim().Split(',');
            List<int> even = new List<int>();
            List<int> odd = new List<int>();
            foreach(var s in strings)
            {
                try
                {
                    int num = int.Parse(s);
                    if ( (num) % 2 == 0)
                    {
                        even.Add(num);
                    }
                    else
                    {
                        odd.Add(num);
                    }
                }
                catch(FormatException)
                {
                    Console.WriteLine( s +" is not a number, returning to menu.");
                    return;
                }
            }
            odd.Sort();
            even.Sort();
            Console.WriteLine("Odd numbers:");
            foreach(int num in odd)
            {
                Console.Write(num);
            }

            Console.WriteLine("even numbers:");
            foreach(int num in even)
            {
                Console.Write(num);
            }

        }
        // 15
        void AdditionCommaSeparatedNumbers()
        {
            Console.Clear();
            Console.WriteLine("Task 14 ");
            Console.WriteLine("Enter a list of numbers with a comma as a seperator.");
            Console.WriteLine("Example: 1,2,3 \nOutput: 6");
            String input = Console.ReadLine();
            if (input == null) {
                Console.Write("Nothing inputted , returning to menu.");
                return;
            }

            String[] strings = input.Trim().Split(',');
            double num = 0;
            foreach(var s in strings)
            {
                try
                {
                    num += double.Parse(s);
                }
                catch(FormatException)
                {
                    Console.WriteLine( s +" is not a number, returning to menu.");
                }
            }
            Console.WriteLine("Output: " + num);
        }


        // 16
        public void CreateTwoCharacthers()
        {
            Console.Clear();
            Console.WriteLine("Task 16 , create two characthers with random atribute values.");
            Console.WriteLine("Enter the name of the first characther.");
            var characther1 = new Character(Console.ReadLine());
            Console.WriteLine("Enter the name of the second characther.");
            var characther2 = new Character(Console.ReadLine());

            Console.WriteLine("**** Characther 1 ****");
            Console.WriteLine(characther1.ToString());
            Console.WriteLine("**** Characther 2 **** ");
            Console.WriteLine(characther2.ToString());
        }

        public class Character {

            private String name;
            private int health;
            private int strength;
            private int luck;

            public String Name { get; set; }
            public int Health { get; set; }
   
            public int Strength { get; set; }
      
            public int Luck { get; set; }
            public Character(String name)
            { 
                var rng = new Random();
                Luck = rng.Next(1, 10);
                Health = rng.Next(50, 100);
                Strength = rng.Next(5, 20);
                Name =  name.Trim();
            }

            public override String ToString()
            {
                return $"Name: {Name}\nHealth: {Health}\nStrength: {Strength}\nLuck: {Luck}";
            }
        }

    }
}
